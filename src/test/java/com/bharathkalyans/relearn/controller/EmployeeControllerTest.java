package com.bharathkalyans.relearn.controller;

import com.bharathkalyans.relearn.dto.EmployeeDto;
import com.bharathkalyans.relearn.model.Employee;
import com.bharathkalyans.relearn.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.*;


@WebMvcTest(EmployeeController.class)
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    EmployeeDto employeeOne;
    EmployeeDto employeeTwo;
    Employee testEmployee;
    List<EmployeeDto> employees = new ArrayList<>();

    @BeforeEach
    void setUp() {
        employeeOne = new EmployeeDto(1, "Bharath", "Kalyan S", "bharath@mail.com");
        employeeTwo = new EmployeeDto(2, "Ananth", "Reddy", "ananth@mail.com");
        testEmployee = new Employee(1, "Bharath", "Kalyan S", "bharath@mail.com");
        employees.add(employeeOne);
        employees.add(employeeTwo);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetAllEmployees() throws Exception {
        when(employeeService.getAllEmployees()).thenReturn(employees);
        this.mockMvc.perform(get("/api/v1/employees"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testGetEmployeeById() throws Exception {
        when(employeeService.getEmployeeById(1L)).thenReturn(employeeOne);
        when(employeeService.getEmployeeById(2L)).thenReturn(employeeTwo);

        this.mockMvc.perform(get("/api/v1/employees/" + "1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.firstName", is("Bharath")))
                .andExpect(jsonPath("$.lastName", is("Kalyan S")))
                .andExpect(jsonPath("$.emailId", is("bharath@mail.com")));

        this.mockMvc.perform(get("/api/v1/employees/" + "2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.firstName", is("Ananth")))
                .andExpect(jsonPath("$.lastName", is("Reddy")))
                .andExpect(jsonPath("$.emailId", is("ananth@mail.com")));
    }

    @Test
    void testGetEmployeeByFirstName() throws Exception {
        when(employeeService.getEmployeeByFirstName("Ananth")).thenReturn(employeeTwo);
        this.mockMvc.perform(get("/api/v1/employees/name/" + "Ananth"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(2)))
                .andExpect(jsonPath("$.firstName", is("Ananth")))
                .andExpect(jsonPath("$.lastName", is("Reddy")))
                .andExpect(jsonPath("$.emailId", is("ananth@mail.com")));
    }

    @Test
    void testCreateEmployee() throws Exception {

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(employeeOne);

        when(employeeService.createEmployee(employeeOne)).thenReturn(testEmployee);

        this.mockMvc.perform(
                        post("/api/v1/employees")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                )
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    void testUpdateEmployeeById() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(employeeOne);

        when(employeeService.updateEmployeeById(1L, employeeOne)).thenReturn(employeeOne);
        this.mockMvc.perform(
                        put("/api/v1/employees/" + "1")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteEmployeeById() throws Exception {
        when(employeeService.deleteEmployeeById(1L)).thenReturn("Employee deleted");

        this.mockMvc.perform(delete("/api/v1/employees/1"))
                .andDo(print())
                .andExpect(status().isOk());

    }
}