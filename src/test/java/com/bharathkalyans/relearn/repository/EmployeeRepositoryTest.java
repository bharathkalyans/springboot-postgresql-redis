package com.bharathkalyans.relearn.repository;


import com.bharathkalyans.relearn.model.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
class EmployeeRepositoryTest {

    @Autowired
    private EmployeeRepository employeeRepository;
    private Employee employee;
    private List<Employee> employees;

    @BeforeEach
    void setUp() {
        employee = new Employee(1, "Bharath", "Kalyan", "bharath@mail.com");
        employees = new ArrayList<>(List.of(
                new Employee(2, "Ananth", "Reddy", "ananth@mail.com"),
                new Employee(3, "Rameshwaran", "Singh", "ram.singh@mail.com"),
                new Employee(4, "Laxman", "Rathod", "laxma@mail.com")
        ));

        employeeRepository.save(employee);
        employeeRepository.saveAll(employees);
    }

    @Test
    void testFindByName() {
        Optional<Employee> findEmployee = employeeRepository.findByFirstName("Bharath");
        assertTrue(findEmployee.isPresent(), "Employee Not Found!!!");
        findEmployee.ifPresent(employee1 -> {
            assertThat(employee1.getFirstName()).isEqualTo(employee.getFirstName());
            assertThat(employee1.getEmailId()).isEqualTo(employee.getEmailId());
        });

    }

    @Test
    void testFindByName_NotFound() {
        Optional<Employee> findEmployee = employeeRepository.findByFirstName("Srinivasan");
        Optional<Employee> findEmployee2 = employeeRepository.findByFirstName("Sujatha");
        assertThat(findEmployee).isNotPresent();
        assertThat(findEmployee2).isNotPresent();
    }

    @AfterEach
    void tearDown() {
        employee = null;
        employees = null;
        employeeRepository.deleteAll();
    }
}
