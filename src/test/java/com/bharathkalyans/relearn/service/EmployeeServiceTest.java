package com.bharathkalyans.relearn.service;

import com.bharathkalyans.relearn.dto.EmployeeDto;
import com.bharathkalyans.relearn.model.Employee;
import com.bharathkalyans.relearn.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@Slf4j
class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private ModelMapper modelMapper;

    private EmployeeService employeeService;
    AutoCloseable autoCloseable;
    EmployeeDto employeeDto;
    Employee testEmployee;

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        employeeService = new EmployeeService(employeeRepository, modelMapper);
        employeeDto = new EmployeeDto(1L, "Bharath", "Kalyan", "bharath@mail.com");
        testEmployee = new Employee(1L, "Bharath", "Kalyan", "bharath@mail.com");

    }

    @Test
    void testModelMapping() {
        ModelMapper mp = new ModelMapper();
        EmployeeDto empDto = mp.map(testEmployee, EmployeeDto.class);
        log.info("Converted Entity to DTO {}", empDto);
        assertThat(empDto.getFirstName()).isEqualTo(employeeDto.getFirstName());
        assertThat(empDto.getId()).isEqualTo(employeeDto.getId());
    }

    @Test
    void testCreateEmployee() {
        mock(Employee.class);
        mock(EmployeeService.class);
        mock(ModelMapper.class);
        mock(EmployeeDto.class);

        when(modelMapper.map(employeeDto, Employee.class)).thenReturn(testEmployee);
        when(employeeRepository.save(testEmployee)).thenReturn(testEmployee);

        Employee employee = employeeService.createEmployee(employeeDto);
        assertThat(employee.getLastName()).isEqualTo(employeeDto.getLastName());
        assertThat(employee.getFirstName()).isEqualTo(employeeDto.getFirstName());
    }

    @Test
    void testGetAllEmployees() {
        mock(Employee.class);
        mock(EmployeeService.class);
        mock(ModelMapper.class);
        mock(EmployeeDto.class);

        when(modelMapper.map(employeeDto, Employee.class)).thenReturn(testEmployee);
        when(employeeRepository.findAll()).thenReturn(List.of(testEmployee));
        List<EmployeeDto> employeeList = employeeService.getAllEmployees();
        assertThat(employeeList.size()).isPositive();
    }

    @Test
    void testGetEmployeeByFirstName() {
        mock(Employee.class);
        mock(EmployeeService.class);
        mock(ModelMapper.class);
        mock(EmployeeDto.class);


        when(employeeRepository.findByFirstName(employeeDto.getFirstName())).thenReturn(Optional.of(testEmployee));
        when(modelMapper.map(testEmployee, EmployeeDto.class)).thenReturn(employeeDto);
        EmployeeDto employee = employeeService.getEmployeeByFirstName(employeeDto.getFirstName());
        assertThat(employee.getFirstName()).isNotBlank();
        assertThat(employee.getFirstName()).isEqualTo(employeeDto.getFirstName());
    }

    @Test
    void testGetEmployeeById() {
        mock(Employee.class);
        mock(EmployeeService.class);
        mock(ModelMapper.class);
        mock(EmployeeDto.class);

        when(employeeRepository.findById(1L)).thenReturn(Optional.of(testEmployee));
        when(modelMapper.map(testEmployee, EmployeeDto.class)).thenReturn(employeeDto);

        EmployeeDto employee = employeeService.getEmployeeById(1L);
        assertThat(employee).isEqualTo(employeeDto);

    }


    @Test
    void testUpdateEmployeeById() {
        mock(Employee.class);
        mock(EmployeeService.class);
        mock(ModelMapper.class);
        mock(EmployeeDto.class);

        when(employeeRepository.findById(1L)).thenReturn(Optional.of(testEmployee));
        EmployeeDto employee = employeeService.updateEmployeeById(1L, employeeDto);
        assertThat(employee).isEqualTo(employeeDto);
        assertThat(employee.getEmailId()).isEqualTo(testEmployee.getEmailId());
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

}