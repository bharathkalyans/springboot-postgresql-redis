package com.bharathkalyans.relearn.model;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import org.hibernate.validator.constraints.Length;


@NoArgsConstructor
@AllArgsConstructor
@Entity
@Setter
@Getter
@Table(name = "employees")
public class Employee {
    @Id
    @SequenceGenerator(name = "employee_sequence_generator", sequenceName = "employee_sequence_generator", initialValue = 101, allocationSize = 1)
    @GeneratedValue(generator = "employee_sequence_generator", strategy = GenerationType.AUTO)
    private long id;

    @Length(min = 4, max = 20, message = "Length of the first name is not matching!!")
    @NotBlank(message = "First Name required")
    private String firstName;
    private String lastName;

    @NotBlank(message = "email id required 😕")
    @Email(message = "Invalid email details", regexp = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,3}")
    private String emailId;

}
