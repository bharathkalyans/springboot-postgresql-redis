package com.bharathkalyans.relearn.controller;


import com.bharathkalyans.relearn.dto.EmployeeDto;
import com.bharathkalyans.relearn.model.Employee;
import com.bharathkalyans.relearn.service.EmployeeService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "api/v1/")
@Tag(name = "Employee")
@Slf4j
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employees")
    @Operation(
            description = "Retrieve details of Employees",
            summary = "All the details of employees will be fetched from the database.",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Success"
                    ),
                    @ApiResponse(
                            responseCode = "404",
                            description = "Unable to Employees Fetch / Invalid EndPoint "
                    )
            }
    )
    public ResponseEntity<List<EmployeeDto>> getAllEmployees() {
        log.info("GET -- ALL EMPLOYEES CALLED");
        return ResponseEntity.ok(employeeService.getAllEmployees());
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<EmployeeDto> getEmployeeById(@PathVariable Long id) {
        log.info("--- CALLING SERVICE TO FETCH DATA FOR EMPLOYEE ID : {} ----", id);
        return ResponseEntity.ok(employeeService.getEmployeeById(id));
    }

    @GetMapping("/employees/name/{firstName}")
    public ResponseEntity<EmployeeDto> getEmployeeByFirstName(@PathVariable String firstName) {
        log.info("--- CALLING SERVICE TO FETCH DATA FOR EMPLOYEE FIRST_NAME : {} ----", firstName);
        return ResponseEntity.ok(employeeService.getEmployeeByFirstName(firstName));
    }

    @PostMapping("/employees")
    public ResponseEntity<Employee> createEmployee(@Valid @RequestBody EmployeeDto employee) {
        return ResponseEntity.ok(employeeService.createEmployee(employee));
    }

    @PutMapping("/employees/{id}")
    @Hidden
    public ResponseEntity<EmployeeDto> updateEmployeeById(@PathVariable Long id, @RequestBody EmployeeDto employee) {
        return ResponseEntity.ok(employeeService.updateEmployeeById(id, employee));
    }

    @DeleteMapping("/employees/{id}")
    @Hidden
    public ResponseEntity<String> deleteEmployeeById(@PathVariable Long id) {
        return ResponseEntity.ok(employeeService.deleteEmployeeById(id));
    }

}
