package com.bharathkalyans.relearn.service;

import com.bharathkalyans.relearn.dto.EmployeeDto;
import com.bharathkalyans.relearn.exception.ResourceNotFoundException;
import com.bharathkalyans.relearn.model.Employee;
import com.bharathkalyans.relearn.repository.EmployeeRepository;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final ModelMapper modelMapper;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, ModelMapper modelMapper) {
        this.employeeRepository = employeeRepository;
        this.modelMapper = modelMapper;
    }

    @Cacheable("EmployeeDtoList")
    public List<EmployeeDto> getAllEmployees() {
        log.info("--- CALLING DB TO FETCH DATA ----");
        List<Employee> employeeList = employeeRepository.findAll();

        return employeeList.stream()
                .map(value -> modelMapper.map(value, EmployeeDto.class))
                .collect(Collectors.toList());
    }

    @Cacheable(value = "Employee", key = "#firstName")
    public EmployeeDto getEmployeeByFirstName(String firstName) {
        log.info("--- CALLING DB TO FETCH DATA FOR EMPLOYEE FIRST_NAME : {} ----", firstName);
        Optional<Employee> employee = employeeRepository.findByFirstName(firstName);
        if (employee.isPresent()) {
            return employee.map(value ->
                    modelMapper.map(employee.get(), EmployeeDto.class)
            ).get();
        } else {
            log.error("Employee with first name {} not Found", firstName);
            throw new ResourceNotFoundException("Employee Not Found with given FirstName");
        }
    }

    @Cacheable(value = "EmployeeDto", key = "#id")
    public EmployeeDto getEmployeeById(Long id) {
        log.info("--- [IN SERVICE ] CALLING DB TO FETCH DATA FOR EMPLOYEE ID : {} ----", id);
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isPresent()) {
            return employee.map(value ->
                    modelMapper.map(employee.get(), EmployeeDto.class)
            ).get();
        } else {
            log.error("Employee with Id {} not Found", id);
            throw new ResourceNotFoundException("Employee does not Exist!");
        }
    }

    @Caching(
            evict = {
                    @CacheEvict(value = "EmployeeDtoList", allEntries = true)
            }
    )
    public Employee createEmployee(EmployeeDto employeeDto) {
        Employee employee = modelMapper.map(employeeDto, Employee.class);
        return employeeRepository.save(employee);
    }


    @Caching(put = {
            @CachePut(value = "EmployeeDto", key = "#id")},
            evict = {@CacheEvict(value = "EmployeeDtoList", allEntries = true)}
    )
    public EmployeeDto updateEmployeeById(Long id, EmployeeDto employeeDto) {
        Optional<Employee> optionalEmployee = employeeRepository.findById(id);

        if (optionalEmployee.isPresent()) {
            Employee existingEmployee = optionalEmployee.get();
            existingEmployee.setFirstName(employeeDto.getFirstName());
            existingEmployee.setLastName(employeeDto.getLastName());
            existingEmployee.setEmailId(employeeDto.getEmailId());

            employeeRepository.save(existingEmployee);
            return employeeDto;
        } else {
            log.error("Employee with Id {} not Found", id);
            throw new ResourceNotFoundException("Employee Not Found!");
        }
    }


    @Caching(evict = {
            @CacheEvict(value = "EmployeeDtoList", allEntries = true),
            @CacheEvict(value = "EmployeeDto", key = "#id")
    })
    public String deleteEmployeeById(Long id) {
        employeeRepository.deleteById(id);
        return "Employee with Id : " + id + " Deleted";
    }
}
