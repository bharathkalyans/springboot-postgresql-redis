# STAGE 1
#  FROM openjdk:17-jdk-slim AS builder
#  LABEL maintainer="Bharath Kalyan S"
#  WORKDIR /app
#  COPY . .
#  RUN ./mvnw clean
#  RUN ./mvnw package

#STAGE 2
FROM openjdk:17-jdk-slim AS runtime
WORKDIR /app
COPY target/relearn-spring-boot.jar app/target/relearn-spring-boot.jar
#COPY --from=builder /app/target/relearn-spring-boot.jar .
EXPOSE 8080
CMD ["java", "-jar", "relearn-spring-boot.jar"]
